pipeline {
    agent none
    environment {
        TEAM="RK99S2TW4N"
        CLOUD = credentials("nextcloud")
        KEYCHAIN = credentials("macos-secret")
        LANG="en_US.UTF-8"
    }
    parameters {
        choice(name: 'APP_PLATFORM', choices: ['Android', 'iOS'])
        string(name: 'APP_ICON', defaultValue: '', description: 'Application Icon\nSource: AppIcon > android')
    }
    options {
        disableConcurrentBuilds()
        timeout(time: 15, unit: 'MINUTES')
        skipDefaultCheckout()
    }
    stages {
        stage("Prepare") {
            agent {
                label params.APP_PLATFORM
            }
            steps {
                cleanWs()
                checkout scm
                script {
                    if(params.APP_ICON != '') {
                        sh "chmod +x ./stage-icon.sh && ./stage-icon.sh"
                        if (fileExists('./AppIcons.zip')) {
                            unzip zipFile: 'AppIcons.zip', dir: './AppIcons'
                            if(params.APP_PLATFORM == 'Android') 
                            {
                                sh "cp -rf ./AppIcons/android/* ./android/app/src/main/res/"
                            } else if(params.APP_PLATFORM == 'iOS') {
                                sh "cp -rf ./AppIcons/Assets.xcassets/* ./ios/TestIconApp/Images.xcassets/"
                            }   
                        } 
                    } else {
                        echo "App is using default icon"
                    }
                }
            }
        }
          stage("Install") {
            agent {
                label params.APP_PLATFORM
            }
            steps {
               nodejs(nodeJSInstallationName: 'Node 18') {
                   script {
                        sh "npm install"
                        if(params.APP_PLATFORM == "iOS") {
                            sh "cd ios && /usr/local/bin/pod install"
                        }
                   }
               }
           }
        }
        stage("Build") {
            agent {
                label params.APP_PLATFORM
            }
            steps {
                nodejs(nodeJSInstallationName: 'Node 18') {
                    sh "chmod +x stage-build.sh && ./stage-build.sh"
                }
            }
        }
        stage("Publish") {
            agent {
                label params.APP_PLATFORM
            }
            steps {
                sh "chmod +x stage-publish.sh && ./stage-publish.sh"
            }
        }

    }
}