#!/bin/bash
if [ "$APP_PLATFORM" == "Android" ]
then
  APK_RELEASE="${WORKSPACE}/android/app/build/outputs/apk/release/app-release.apk"
  OUTPUT_FILE="TestIcon-$BUILD_NUMBER.apk"
  echo "Upload $OUTPUT_FILE to share.twsbp.com"
  curl -X PUT -u "$CLOUD_USR:$CLOUD_PSW" "http://share.twsbp.com/remote.php/dav/files/devop/Debug/android/$OUTPUT_FILE" -T "$APK_RELEASE"
else
  IOS_DIR="${WORKSPACE}/ios"
  OUTPUT_FILE="TestIcon-$BUILD_NUMBER.ipa"
  echo "Upload $OUTPUT_FILE to share.twsbp.com"
  curl -X PUT -u "$CLOUD_USR:$CLOUD_PSW" "http://share.twsbp.com/remote.php/dav/files/devop/Debug/ios/$OUTPUT_FILE" -T "$IOS_DIR/release.ipa/TestIconApp.ipa"
fi
