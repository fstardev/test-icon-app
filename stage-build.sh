#!/bin/bash
if [ "$APP_PLATFORM" == "Android" ]
then
  cd android &&  ./gradlew app:assembleRelease
else
  IOS_DIR="${WORKSPACE}/ios"
  TARGET_ARCHIVE="$IOS_DIR/release.xcarchive"
  TARGET_IPA="$IOS_DIR/release.ipa"
  security unlock-keychain -p "$KEYCHAIN" /Users/twsbp/Library/Keychains/login.keychain-db
  xcodebuild -workspace "$IOS_DIR/TestIconApp.xcworkspace" -list -UseModernBuildSystem=NO -configuration Release -allowProvisioningUpdates build
  xcodebuild archive -workspace "$IOS_DIR/TestIconApp.xcworkspace" -scheme TestIconApp -archivePath "$TARGET_ARCHIVE" CODE_SIGN_STYLE="Automatic"  DEVELOPMENT_TEAM="$TEAM" -UseModernBuildSystem=NO -configuration Release -allowProvisioningUpdates
  xcodebuild -exportArchive -archivePath "$TARGET_ARCHIVE" -exportPath "$TARGET_IPA" -exportOptionsPlist "$IOS_DIR/exportOptions.plist"  -UseModernBuildSystem=NO -allowProvisioningUpdates
fi